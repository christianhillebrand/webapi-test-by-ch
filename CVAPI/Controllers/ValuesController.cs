﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CVAPI.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class MeController : Controller
    {
        [HttpGet]
        public IDictionary<string,string> Get()
        {

            var ret = new Dictionary<string, string>();
            ret.Add("NAME", this.User.Claims.Where(x => x.Type == "name").First().Value);
            ret.Add("API", "https://cvapicustomer1.azurewebsites.net");

            
            return ret;
        }
    }
    
}
